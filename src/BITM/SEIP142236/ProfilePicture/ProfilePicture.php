<?php

namespace App\ProfilePicture;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;


class ProfilePicture extends DB
{
    public $id;
    public $name;
    public $picture_link;


    public function __construct()
    {
        parent::__construct();
        if(!isset($_SESSION))
            session_start();
    }

    public function setData($postVaribaleData=NULL)
    {
        if(array_key_exists("id",$postVaribaleData))
        {
            $this->id = $postVaribaleData['id'];
        }
        if(array_key_exists("name",$postVaribaleData))
        {
            $this->name = $postVaribaleData['name'];
        }
        $this->picture_link = time().$_FILES['profile_photo']['name'];
        $temporary_location = $_FILES['profile_photo']['tmp_name'];

        move_uploaded_file($temporary_location,'../../../picture/'.$this->picture_link);

    }//end of set data
    public function store()
    {
        $arrData = array($this->name,$this->picture_link);
        $sql = "INSERT into profile_picture(profile_name,profile_picture) VALUES (?,?)";
        $STH = $this->dbh->prepare($sql);
        $result = $STH->execute($arrData);

        if($result) {
            //Message::setMessage("Success!!Data has been inserted successfully ;)");
            Message::message("Success!!Data has been inserted successfully ;)");
        }
        else {
            //Message::setMessage("Failed!! Data has not been inserted successfully :(");
            Message::message("Failed!! Data has not been inserted successfully :(");
        }

        Utility::redirect('create.php');


    }

    public function index($fetchMode='ASSOC'){

        $STH = $this->dbh->query('SELECT * from profile_picture ORDER BY profile_pic_id ASC');

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();

    public function view($fetchMode='ASSOC'){

        $sql = 'SELECT * from profile_picture where profile_pic_id='.$this->id;

        $STH = $this->dbh->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData  = $STH->fetch();
        return $arrOneData;


    }// end of view();

}