<head>
    <link rel="stylesheet" href="../resource/css/bootstrap.min.css">
    <link rel="stylesheet" href="../resource/css/font-awesome.min.css">
    <script src="../resource/js/jquery-1.11.1.min.js"></script>
    <script src="../resource/js/bootstrap.min.js"></script>
</head>

<?php
require_once ('../../../vendor/autoload.php');
use App\SummaryOfOrganization\SummaryOfOrganization;
use App\Message\Message;

$objSummary = new SummaryOfOrganization();

$allData = $objSummary->index('obj');

$serial = 1;
echo "<table border='5px' align='center'>";

echo "<th> Serial </th>";
echo "<th> ID </th>";
echo "<th> Organization name </th>";
echo "<th> Summary </th>";
echo "<th> Action </th>";


foreach($allData as $oneData){
    echo "<tr style='height: 50px'>";
    echo "<td>".$serial."</td>";

    echo "<td>".$oneData->summary_organizaton_id."</td>";
    echo "<td>".$oneData->organization_name."</td>";
    echo "<td style='width: 50%'>".$oneData->summary."</td>";


    echo "<td>";

    echo "<a href='view.php?id=$oneData->summary_organizaton_id'><button class='btn btn-info'>View</button></a> ";
    echo "<a href='edit.php?id=$oneData->summary_organizaton_id'><button class='btn btn-primary'>Edit</button></a> ";
    echo "<a href='delete.php?id=$oneData->summary_organizaton_id'><button class='btn btn-warning'>Delete</button></a> ";
    echo "<a href='trash.php?id=$oneData->summary_organizaton_id'><button class='btn btn-danger'>Trash</button></a> ";


    echo "</td>";

    echo "</tr>";

    $serial++;
}

echo "</table>";
