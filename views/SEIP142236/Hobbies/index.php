<head>
    <link rel="stylesheet" href="../resource/css/bootstrap.min.css">
    <link rel="stylesheet" href="../resource/css/font-awesome.min.css">
    <script src="../resource/js/jquery-1.11.1.min.js"></script>
    <script src="../resource/js/bootstrap.min.js"></script>
</head>

<?php
require_once ('../../../vendor/autoload.php');
use App\Hobbies\Hobbies;
use App\Message\Message;

$objHobby = new Hobbies();

$allData = $objHobby->index('obj');

$serial = 1;
echo "<table border='5px' align='center'>";

echo "<th> Serial </th>";
echo "<th> ID </th>";
echo "<th> Name </th>";
echo "<th> Hobbies </th>";
echo "<th> Action </th>";


foreach($allData as $oneData){
    echo "<tr style='height: 50px'>";
    echo "<td>".$serial."</td>";

    echo "<td>".$oneData->hobby_id."</td>";
    echo "<td>".$oneData->person_name."</td>";
    echo "<td>".$oneData->hobbies."</td>";


    echo "<td>";

    echo "<a href='view.php?id=$oneData->hobby_id'><button class='btn btn-info'>View</button></a> ";
    echo "<a href='edit.php?id=$oneData->hobby_id'><button class='btn btn-primary'>Edit</button></a> ";
    echo "<a href='delete.php?id=$oneData->hobby_id'><button class='btn btn-warning'>Delete</button></a> ";
    echo "<a href='trash.php?id=$oneData->hobby_id'><button class='btn btn-danger'>Trash</button></a> ";


    echo "</td>";

    echo "</tr>";

    $serial++;
}

echo "</table>";
