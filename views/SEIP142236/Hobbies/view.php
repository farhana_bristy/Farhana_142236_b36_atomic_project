<?php

require_once("../../../vendor/autoload.php");

use App\Hobbies\Hobbies;

$objHobbies  =  new Hobbies();
$objHobbies->setData($_GET);
$oneData= $objHobbies->view("obj");

echo $oneData->hobby_id.'<br>';
echo $oneData->person_name.'<br>';
echo $oneData->hobbies.'<br>';
