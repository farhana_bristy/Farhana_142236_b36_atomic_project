
<?php


require_once("../../../vendor/autoload.php");

use App\Message\Message;
use App\BookTitle\BookTitle;

$objBookTitle= new BookTitle();

$objBookTitle->setData($_GET);

$oneData = $objBookTitle->view("obj");

//var_dump($oneData);die;

//echo Message::getMessage();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Edit</title>
    <link rel="stylesheet" href="../resource/css/bootstrap.min.css">
    <script src="../resource/js/bootstrap.min.js"></script>
</head>
<style>
    body{
        padding-top: 20px;
        background-color: #0f0f0f;
        background: url("../resource/img/bg1.jpg") no-repeat center center fixed;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
    }
</style>
<body>


<div class="container">
    <div class="row centered-form text-center" style="margin-top: 12%">
        <p style="color: #31b0d5;text-align: center">
            <?php
            echo Message::message();
            ?>
        </p>
        <h2 style="color: #a6e1ec">Edit Form</h2>
        <div class="col-xs-12 col-sm-8 col-md-4 col-sm-offset-2 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Book Title</h3>
                </div>
                <div class="panel-body">
                    <form role="form" action="update.php" method="post">
                        <div class="form-group">
                            <label for="tt"></label>
                            <input type="hidden" name="id" value="<?php echo $oneData->book_id; ?>">
                            <input type="text" name="book_title" id="first_name" class="form-control input-sm" value="<?php echo $oneData->book_title; ?>">
                        </div>
                        <div class="form-group">
                            <input type="text" name="author_name" id="email" class="form-control input-sm" value="<?php echo $oneData->author_name; ?>">
                        </div>
                        <input type="submit" value="Update" class="btn btn-info btn-block">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
